;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; These are the packages that I use for most of my needs.
;; JP Custom Packages

(package! rainbow-mode)
(package! org-auto-tangle)
(package! yasnippet)
(package! company-box)
(package! key-chord)
(package! olivetti)
(package! logos)
(package! denote)
(package! evil-nerd-commenter)
(package! pdf-tools)
(package! nyan-mode)
(package! lin)
(package! evil-visual-replace)
(package! fill-column-indicator)
