;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;;
;; ****************************************************************************
;;                    Javier Pacheco DOOM Configuration
;; ****************************************************************************

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Javier Pacheco"
      user-mail-address "jpacheco@cock.li")

;; The actual font
;; (setq doom-font (font-spec :family "Iosevka Term SS04" :size 12 :weight 'medium))
(setq doom-font (font-spec :family "Iosevka Term SS04" :weight 'medium))

(setq olivetti-body-width 0.45
      olivetti-minimum-body-width 70
      olivetti-recall-visual-line-mode-entry-state t)

(let ((map global-map))
  (define-key map (kbd "<f1>") #'olivetti-mode)
  (define-key map (kbd "<f2>") #'logos-backward-page-dwim)
  (define-key map (kbd "<f3>") #'logos-forward-page-dwim)
  (define-key map (kbd "<f4>") #'calculator)
  (define-key map (kbd "S-<f4>") #'calendar)
  (define-key map (kbd "S-<f1>") #'eww))



;; ****************************************************************************
;;                               Initial Settings
;; ****************************************************************************

(setq default-directory "~/.doom.d/")
(add-hook 'emacs-startup-hook 'toggle-frame-maximized)
(add-hook 'emacs-startup-hook 'yas-global-mode)
(global-eldoc-mode -1)
(nyan-mode)
(setq-default fill-column 100)
(setq-default fill-column-width 9)
(global-display-fill-column-indicator-mode)

(defun my-toggle-vi-tilde-fringe-mode ()
  "toggle vi-tilde-fringe-mode"
  (remove-hook 'text-mode-hook #'vi-tilde-fringe-mode))

(add-hook 'olivetti-mode-on-hook (lambda () (setq display-line-numbers nil)))
(add-hook 'olivetti-mode-on-hook 'my-toggle-vi-tilde-fringe-mode)
(add-hook 'olivetti-mode-on-hook 'logos-narrow-dwim)
(add-hook 'olivetti-mode-off-hook (lambda () (setq display-line-numbers 'relative)))
(add-hook 'olivetti-mode-off-hook 'my-toggle-vi-tilde-fringe-mode)
(add-hook 'olivetti-mode-off-hook 'logos-narrow-dwim)

(add-hook 'buffer-list-update-hook 'rainbow-mode 1)

(remove-hook 'text-mode-hook #'vi-tilde-fringe-mode)
(remove-hook 'prog-mode-hook #'vi-tilde-fringe-mode)

(setq modus-themes-paren-match '(bold intense))

(setq native-comp-async-report-warnings-errors 'silent)

;; numbers style
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative
      display-line-numbers-grow-only t)

;; Prevent lines from being shifted when order of magnitude increases.
(setq display-line-numbers-width-start t)

;; Disable line numbers for some modes
(dolist (mode '(term-mode-hook
                shell-mode-hook
                vterm-mode-hook
                ;; org-mode-hook
                org-agenda-mode-hook
                pdf-view-mode-hook
                treemacs-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;;To avoid exit promt. and other fancy stuff
(setq confirm-kill-emacs nil)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(setq modus-themes-mode-line (list 'moody 'borderless ))
(setq modus-themes-hl-line '(underline))
(setq modus-themes-region '(extend bg-only))
(setq modus-themes-org-blocks 'tinted-background) ; {nil,'gray-background,'tinted-background}

(custom-set-faces
  '(modus-themes-hl-line ((t (:background nil))))
  '(hl-line ((t (:background nil))))
  '(hl-line ((t (:underline "#458588"))))
  '(modus-vivendi-theme-bg-active ((t (:background "#458588"))))
  '(line-number ((t (:background nil))))
  '(org-document-title ((t (:inherit default :weight bold :font "Iosevka Term SS04" :underline t :height 1.00))))
  '(line-number-current-line ((t (:background "#458588" :underline "#458588")))))

;;Theme and fonts
(setq doom-theme 'modus-vivendi)
(blink-cursor-mode)
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

(setq evil-vsplit-window-right t)
(setq evil-split-window-below t)

;; Transparency
;; (set-frame-parameter (selected-frame) 'alpha '(90 . 90))
;; (add-to-list 'default-frame-alist '(alpha . (90 . 90)))



;; ****************************************************************************
;;                          Dashboard configuration
;; ****************************************************************************

(let ((alternatives '("1.png"
                      )))
  (setq fancy-splash-image
        (concat doom-private-dir "img/"
                (nth (random (length alternatives)) alternatives))))

(remove-hook! '+doom-dashboard-functions
#'doom-dashboard-widget-footer
#'doom-dashboard-widget-loaded
#'doom-dashboard-widget-shortmenu)

(add-hook!   '+doom-dashboard-mode-hook (hl-line-mode -1) (hide-mode-line-mode 1))

(add-hook! '+doom-dashboard-functions
           :append
           (insert "\n" (+doom-dashboard--center +doom-dashboard--width
                                                 "“Make it work, make it right, make it fast.” – Kent Beck")))



;; ****************************************************************************
;;                                  Keybinds
;; ****************************************************************************
;;
;; Open the dashboard
;;
;;This is an example of keymapping:
;; (map! :leader
;;       (:prefix ("0" . "dashboard")
;;         :desc "Open th doom dashboard"
;;         "1" #'+doom-dashboard/open))

(global-set-key (kbd "C-SPC") 'switch-to-buffer) ; switch to buffer

(map! :leader
      (:prefix ("o" . "open")
      :desc "Toggle neotree"
      "e" #'ranger))

(map! :leader
      (:prefix ("o" . "open")
      :desc "Open calendar"
      "c" #'+calendar/open-calendar))

(map! :leader
      ("1" #'+doom-dashboard/open))

(map! :leader
      ("]" #'next-buffer))

(map! :leader
      ("[" #'previous-buffer))

(map! :leader
      (:prefix ("c" . "denote bindings")
        :desc "denote"
        "c" #'denote
        "l" #'denote-link-insert-links-matching-regexp
        "d" #'denote-subdirectory))

(map! :leader
      (:prefix ("g" )
        "d" #'vc-diff))

(defun org-make-olist (arg)
  (interactive "P")
  (let ((n (or arg 1)))
    (when (region-active-p)
      (setq n (count-lines (region-beginning)
                           (region-end)))
      (goto-char (region-beginning)))
    (dotimes (i n)
      (beginning-of-line)
      (insert (concat (number-to-string (1+ i)) ". "))
(forward-line))))

(global-set-key (kbd "M-k") #'move-text-up)
(global-set-key (kbd "M-j") #'move-text-down)

(define-key evil-motion-state-map (kbd "C-w C-c") #'evil-window-delete)
(define-key evil-motion-state-map (kbd "C-h") #'evil-window-left)
(define-key evil-motion-state-map (kbd "C-j") #'evil-window-down)
(define-key evil-motion-state-map (kbd "C-k") #'evil-window-up)
(define-key evil-motion-state-map (kbd "C-l") #'evil-window-right)
(define-key evil-motion-state-map (kbd "M-/") #'evilnc-comment-or-uncomment-lines)
(key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
(key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
(key-chord-define evil-insert-state-map "aa" 'end-of-line)
(key-chord-mode 1)



;; ****************************************************************************
;;                               Org-Mode
;; ****************************************************************************

(after! org
  (setq org-agenda-start-on-weekday nil)
  (setq org-support-shift-select t)
  (setq org-superstar-prettify-item-bullets nil)
  (setq org-log-done 'time)
  (setq org-hide-emphasis-markers t)
  (setq org-log-into-drawer t)
  (setq org-superstar-headline-bullets-list '(?⁖))
  (setq org-ellipsis " ▼ ")
  (setq org-directory "~/.doom.d/org/")
  (setq org-agenda-files
   (apply 'append
          (mapcar
           (lambda
             (directory)
             (directory-files-recursively directory org-agenda-file-regexp))
           '("~/.doom.d/org"))))
  (setq org-todo-keywords
        (quote ((sequence "TODO(t@/!)" "DOING(D@/!)" "|" "DONE(d@/!)")
                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)"))))
  (setq org-emphasis-alist
        '(("*" (bold :slant italic :weight black :foreground "#458588" ))
          ("/" (italic :foreground "#458588" ))
          ("_" (italic :underline t  ))
          ("=" (italic :foreground "#458588"))
          ("~" (italic :foreground "#458588" ))
          ("+" (italic :strike-through nil :foreground "#458888" ))))
  ;; TODO colors
  (setq org-todo-keyword-faces
        '(
          ("TODO" . (:foreground "#d65d0e" :weight italic))
          ("DOING" . (:foreground "#458588" :weight italic))
          ("WAITING" . (:foreground "#98971a" :weight italic))
          ("HOLD" . (:foreground "#d79921" :weight italic))
          ("DONE" . (:foreground "#689d6a" :weight italic))
          ("CANCELLED" . (:foreground "#9d0006" :weight italic))))
  (add-hook 'org-mode-hook 'org-superstar-mode)
  (add-hook 'org-mode-hook 'visual-line-mode)
  (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
  (define-key global-map "\C-A" 'org-agenda)
  (define-key global-map "\C-cc" 'denote)
  (define-key global-map "\C-cl" 'denote-link-insert-links-matching-regexp)
  (define-key global-map "\C-cd" 'denote-subdirectory)
  (define-key global-map "\M-l" 'org-make-olist))

(set-popup-rule! "^\\*Org Agenda" :side 'bottom :size 0.50 :select t :ttl nil)
(set-popup-rule! "^\\*calculator" :side 'left :size 0.50 :select t :ttl nil)



;; ****************************************************************************
;;                               Denote configuration
;; ****************************************************************************

(setq denote-directory "~/.doom.d/notes/"
       denote-known-keywords '("home" "personal"))

(setq pulsar-pulse t)
(setq pulsar-delay 0.05)
(setq pulsar-iterations 10)

(let ((map global-map))
  (define-key map (kbd "C-x L") #'pulsar-pulse-line-cyan))

(pulsar-global-mode 1)



;; ****************************************************************************
;;                            YASNIPPET config
;; ****************************************************************************

(yas-global-mode)
(lsp-ui-mode)
(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)
(setq yas-snippet-dirs '("~/.doom.d/extras/snippets/"))
(setq yas-triggers-in-field t)

(after! lsp-ui
  (setq lsp-ui-doc-en t)
  (setq lsp-ui-doc-show-with-cursor t)
  (setq lsp-ui-doc-position 'right)
  (setq lsp-signature-render-documentation nil)
  (setq lsp-ui-doc-delay 0)
  (setq lsp-ui-doc-max-width 60)
  (setq lsp-ui-doc-max-height 800)
  (setq lsp-ui-sideline-enable nil))

(add-hook 'after-init-hook 'global-company-mode)
(add-hook 'prog-mode-hook #'yas-minor-mode)

;; customize company frontend variables
(setq company-minimum-prefix-length 1)
(setq company-idle-delay 0.5)
(setq company-selection-wrap-around t)
(setq company-tooltip-limit 10)
(setq yas-prompt-functions '(yas-no-prompt))
(define-key yas-minor-mode-map (kbd "S-SPC") 'yas-expand)

(defun check-expansion ()
  (save-excursion
    (if (looking-at "\\_>") t
      (backward-char 1)
      (if (looking-at "\\.") t
        (backward-char 1)
        (if (looking-at "->") t nil)))))

(defun do-yas-expand ()
  (let ((yas-fallback-behavior 'return-nil))
    (yas/expand)))

(defun tab-indent-or-complete ()
  (interactive)
  (if (minibufferp)
      (minibuffer-complete)
    (if (or (not yas-minor-mode)
            (null (do-yas-expand)))
        (if (check-expansion)
            (company-complete-common)
          (indent-for-tab-command)))))

(global-set-key [tab] 'tab-indent-or-complete)

(defun trigger-org-company-complete ()
  "Begins company-complete in org-mode buffer after pressing #+ chars."
  (interactive)
  (if (string-equal "#" (string (preceding-char)))
    (progn
      (insert "+")
      (company-complete))
    (insert "+")))

(eval-after-load 'org '(define-key org-mode-map
               (kbd "+") 'trigger-org-company-complete))

(add-to-list 'company-backends 'company-dabbrev-code)
  (add-to-list 'company-backends 'company-yasnippet)
  (add-to-list 'company-backends 'company-files)

(setq company-minimum-prefix-length 1)
(make-variable-buffer-local 'company-minimum-prefix-length)

(setq company-transformers '(company-sort-by-backend-importance))

;; company-active-map
(use-package! company-box
  :hook (company-mode . company-box-mode))

(after! company-box
  (add-function
   :after
   (symbol-function 'company-box-doc--show)
   (lambda (_ frame)
     (when (frame-visible-p (frame-parameter frame 'company-box-doc-frame))
       (when (not (frame-visible-p (company-box--get-frame)))
         (make-frame-visible (company-box--get-frame)))))))



;; ****************************************************************************
;;                           Org-Capture
;; ****************************************************************************

(setq org-capture-templates
        '(("t" "TODO" entry (file "~/.doom.d/org/agenda.org")
        "* TODO %? \n %U" :empty-lines 1)
        ("s" "Scheduled TODO" entry (file "~/.doom.d/org/agenda.org")
        "* TODO %? \nSCHEDULED: %^t\n  %U" :empty-lines 1)
        ("n" "Note" entry (file "~/.doom.d/org/notes.org")
        "* %? \n %U" :empty-lines 1)
        ("T" "amm TODO" entry (file "~/.doom.d/org/amm.org")
        "* TODO %? \n %U" :empty-lines 1)
        ("S" "amm Scheduled TODO" entry (file "~/.doom.d/org/amm.org")
        "* TODO %? \nSCHEDULED: %^t\n  %U" :empty-lines 1)
        ("N" "amm Note" entry (file "~/.doom.d/org/ammnotes.org")
        "* %? \n %U" :empty-lines 1)))



;; ****************************************************************************
;;                            MU4E config
;; ****************************************************************************
;:TODO Make this works in my main machine... Not in windows, not luck
(setq mail-user-agent 'mu4e-user-agent
      mu4e-sent-folder   "/Sent"
      mu4e-drafts-folder "/Drafts"
      mu4e-refile-folder "/Archive"
      mu4e-spam-folder   "/Junk"
      mu4e-trash-folder  "/Trash")

(setq mu4e-get-mail-command "true")
(setq mu4e-html2text-command "w3m -T text/html")
(setq mu4e-view-html-plaintext-ratio-heuristic most-positive-fixnum)

(setq message-send-mail-function   'smtpmail-send-it
      smtpmail-smtp-server         "mail.cock.li"
      smtpmail-smtp-service        587
      smtpmail-smtp-stream-type    'starttls)

(defun my-mu4e-inbox ()
  "jump to mu4e inbox"
  (interactive)
  (mu4e~headers-jump-to-maildir "/Inbox"))

(map! :leader
      :desc "Jump to mu4e inbox"
      "oi" 'my-mu4e-inbox)



;; ****************************************************************************
;;                            File managment
;; ****************************************************************************


(after! neotree
  (setq neo-smart-open t
        neo-window-fixed-size nil))

(use-package! org-auto-tangle
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config
  (setq org-auto-tangle-default t))



;; *****************************************************************************
;;                           Tests.......
;; *****************************************************************************

;TODO: need to assign a key map to replace the select region, like my personal map in NVIM....
;TODO: make little snippets of code and /require those in here/ like: require 'jp-dashboard.el
