#+title:      Falla caja roja B5
#+date:       [2022-08-09 Tue 12:11]
#+filetags:   :redbin:
#+identifier: 20220809T121115

* No se alarma en disaro de baja:
se revisa programa y no esta reciviendo algunas de las senales para que se cumpla la condicion de /PIECE NO GOOD/

| SENAL | DESCRICION                            |
|-------+---------------------------------------|
| I:0/4 | SEÑAL PARA DISPARAR DE HORNO A BUHLER |
| I:0/5 | PUERTA ABIERTA DE BUHLER              |
| I:0/9 | SENSOR EXPULSOR ADELANTE              |
